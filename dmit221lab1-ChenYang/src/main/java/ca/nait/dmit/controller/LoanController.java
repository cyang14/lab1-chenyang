package ca.nait.dmit.controller;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import ca.nait.dmit.domain.Loan;
import ca.nait.dmit.domain.LoanSchedule;
import util.JSFUtil;

@ManagedBean(name="loanBean")
@ViewScoped
public class LoanController {
	private Loan currentLoan = new Loan();
	private LoanSchedule[] loanScheduleTable;
	private CartesianChartModel categoryModel;
	
	public Loan getCurrentLoan() {
		return currentLoan;
	}
	public void setCurrentLoan(Loan currentLoan) {
		this.currentLoan = currentLoan;
	}
	public LoanSchedule[] getLoanScheduleTable() {
		return loanScheduleTable;
	}
	public void setLoanScheduleTable(LoanSchedule[] loanScheduleTable) {
		this.loanScheduleTable = loanScheduleTable;
	}
	public CartesianChartModel getCategoryModel() {  
        return categoryModel;  
    }
	
	public void setCategoryModel(CartesianChartModel categoryModel) {
		this.categoryModel = categoryModel;
	}
	public LoanController() {  
        createCategoryModel();  
    }  
	private void createCategoryModel() {  
        categoryModel = new CartesianChartModel();  
  
        ChartSeries remainingBalance = new ChartSeries();  
        remainingBalance.setLabel("Remaining Balance");  
        
        for(int paymentNo = 11; paymentNo <= currentLoan.getAmortizationPeriod() * 12 - 1; paymentNo = paymentNo + 12){
        	remainingBalance.set((paymentNo + 1)/12, loanScheduleTable[paymentNo].getRemainingBalance());
        }

        categoryModel.addSeries(remainingBalance);  
        
    }
	
	
	public void calculatePayment() {
		JSFUtil.addInfoMessage("Your montly payment is: " + currentLoan.getMonthlyPayment());
		loanScheduleTable = currentLoan.getLoanScheduleArray();
		createCategoryModel();
		
	}
}
