package ca.nait.dmit.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import ca.nait.dmit.domain.BMI;
import util.JSFUtil;

@ManagedBean(name="bmiBean")
@RequestScoped
public class BMIController {
	private BMI bmi = new BMI();

	public BMI getBmi() {
		return bmi;
	}

	public void setBmi(BMI bmi) {
		this.bmi = bmi;
	}

	public void calculateBMI() {
		JSFUtil.addInfoMessage("Your bmi is: " + bmi.getBMI());
		if(bmi.getBMICategory() == "normal")
		{
			JSFUtil.addInfoMessage("You are cateogrized as being " + bmi.getBMICategory());
		}
		else if(bmi.getBMICategory() == "overweight" || bmi.getBMICategory() == "underweight")
		{
			JSFUtil.addWarningMessage("Warning! You are cateogrized as being " + bmi.getBMICategory());
		}
		else
		{
			JSFUtil.addErrorMessage("Warning! You are cateogrized as being " + bmi.getBMICategory());
		}
	}
	
	
	

}
