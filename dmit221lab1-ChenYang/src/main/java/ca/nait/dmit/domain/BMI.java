
package ca.nait.dmit.domain;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Min;

/**
 * This class is use to calculate a person's body mass index (BMI) and their BMI Category.
 * @author ChenYang
 * @version 2013.05.22
 */
public class BMI {
	
	@Min(value=40,message="Weight must be at least 40lbs")
	private int weight;

	@Min(value=3,message="HeightFeet must be at least 3 feet")
	private int heightFeet;

	@DecimalMax(value="11.99",message="HeightInches must be less than 12 inches")
	private double heightInches;
	
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getHeightFeet() {
		return heightFeet;
	}
	public void setHeightFeet(int height) {
		this.heightFeet = height;
	}
	public double getHeightInches() {
		return heightInches;
	}
	public void setHeightInches(double heightInches) {
		this.heightInches = heightInches;
	}
	
	public BMI(int weight, int heightFeet, double heightInches) {
		super();
		this.weight = weight;
		this.heightFeet = heightFeet;
		this.heightInches = heightInches;
	}
	public BMI() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * Calculate the body mass index (BMI) using the weight and height of the person.
	 * The BMI of a person is calculated using the formula: BMI = 700 * weight / (height * height)
	 * where weight is in pounds and height is in inches. 
	 * @return the body mass index (BMI) value of the person
	 */
	public double getBMI() {
		
		return 703 * getWeight() / (java.lang.Math.pow(getTotalHeight(), 2));
	}
	/**
	 * Get Total Height = HeightFeet * 12 + HeightInches
	 * @return TotalHeight
	 */
	private double getTotalHeight() {
		int heightFeet = getHeightFeet();
		double heightInches = getHeightInches();
		return heightFeet * 12 + heightInches;
	}
	/**
	 * Determines the BMI Category of the person using their BMI value and comparing
	 * it against the following table.
	 * -----------------------------------------
	 * | BMI range			        | BMI Category |
	 * |---------------------------------------|
	 * | < 18.5 			        | underweight	 |
	 * | >= 18.5 and < 25			| normal		   |
	 * | >= 25 and < 30				| overweight	 |
	 * | >= 30				        | obese			   |
	 * -----------------------------------------
	 * @return one of following: underweight, normal, overweight, obese.
	 */
	public String getBMICategory(){
		String BMICategory = "";
		double BMI = getBMI();
		if(BMI < 18.5){
			BMICategory = "underweight";
		}else if(BMI >= 18.5 && BMI <= 24.9){
			BMICategory = "normal";
		}else if(BMI >= 25 && BMI <= 29.9){
			BMICategory = "overweight";
		}else{
			BMICategory = "obese";
		}
		return BMICategory;
	}

}
