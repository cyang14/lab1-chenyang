package ca.nait.dmit.domain;
/**
 * This class is use to calculate the loan.
 * @author ChenYang
 * @version 2013.05.26
 */
public class Loan {
	private double mortgageAmount;
	private double annualInterestRate;
	private int amortizationPeriod;
	
	public double getMortgageAmount() {
		return mortgageAmount;
	}
	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}
	public double getAnnualInterestRate() {
		return annualInterestRate;
	}
	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	public int getAmortizationPeriod() {
		return amortizationPeriod;
	}
	public void setAmortizationPeriod(int amortizationPeriod) {
		this.amortizationPeriod = amortizationPeriod;
	}
	public Loan() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Loan(double mortgageAmount, double annualInterestRate,
			int amortizationPeriod) {
		super();
		this.mortgageAmount = mortgageAmount;
		this.annualInterestRate = annualInterestRate;
		this.amortizationPeriod = amortizationPeriod;
	}
	/**The getMonthlyPayment method will return the monthly 
	 * payment of the loan rounded to 2 decimal places using 
	 * the Monthly Payment formula 	
	 * @return the value of monthlypayment
	 */
	public double getMonthlyPayment()
	{
		return roundTo2Decimals(mortgageAmount * (java.lang.Math.pow((1 + (annualInterestRate/200)), 1.0/6.0) - 1)/(1 - java.lang.Math.pow(java.lang.Math.pow((1 + (annualInterestRate/200)), 1.0/6.0),(-12 * amortizationPeriod))));
	}
	

	/**
	   * Rounds a double value to 2 decimal places
	   * @param valueToRound the value to round
	   * @return the value rounded to 2 decimal places
	   */
	  public static double roundTo2Decimals(double valueToRound)
	  {
	  	return Math.round( valueToRound * 100 ) / 100.0;
	  }
	  /**
	   * The getLoanScheduleArray method will use the formulas 
	   * shown in figure 2 to return the Loan Schedule Table as an array of 
	   * LoadSchedule objects. You will have to round the interest paid, principal paid, 
	   * and remaining balance of each payment to 2 decimal places. 
	   * @return Loan Schedule array
	   */
	  public LoanSchedule[] getLoanScheduleArray()
	  {
	  	// number of payments is the loan term (in years) multiply by the number of months per year (12)
	  	int numberOfPayments = 12 * amortizationPeriod;
	  	LoanSchedule[] loanScheduleArray = new LoanSchedule[ numberOfPayments ];
	  	// set the initial remaining balance is equal to amount borrowed
	  	double remainingBalance = mortgageAmount;
	  	// calculate monthlyPercentageRate
	  	for( int paymentNumber = 1; paymentNumber <= numberOfPayments; paymentNumber++ )
	  	{
	  		// calculate interestPaid and round to 2 decimal places
	  		double interestPaid = (java.lang.Math.pow((1 + (annualInterestRate/200)),1.0/6.0) - 1) * remainingBalance;
	  		interestPaid = roundTo2Decimals(interestPaid);
	  		
	  		// calculate principalPaid and round to 2 decimal places
	  		double principalPaid = getMonthlyPayment() - interestPaid;
	  		principalPaid = roundTo2Decimals(principalPaid);
	  		
	  		// update remainingBalance and round to 2 decimal places
	  		
	  		
	  		// set remainingBalance to zero if it is calculated to be less than zero
	  		if (remainingBalance <= 0){
	  			remainingBalance = 0;
	  		}else if(remainingBalance < getMonthlyPayment())
  			{
	  			principalPaid = remainingBalance;
  			}
	  		
	  		remainingBalance = roundTo2Decimals(remainingBalance - principalPaid);
	  		
	  		
	  		// arrays in Java are 0-index based
	  		loanScheduleArray[ paymentNumber - 1 ] = new LoanSchedule( paymentNumber, interestPaid, principalPaid, remainingBalance );
	  		
	  	}
	  	return loanScheduleArray;
	  }	

}
