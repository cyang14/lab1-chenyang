package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.*;

import org.junit.*;

public class BMITest {
	private static Validator validator;
	  
	  @BeforeClass
	  public static void setUpBeforeClass() throws Exception
	  {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	    validator = factory.getValidator();
	  }

	  @Test
	  public void testGetBMIUnderweigth()
	  {
		BMI bmi = new BMI(100, 5, 6);
		assertEquals(16.1, bmi.getBMI(), 0.05);
		assertTrue(bmi.getBMICategory().equalsIgnoreCase("underweight"));
	  }

	  @Test
	  public void testGetBMINormal()
	  {
		BMI bmi = new BMI(140, 5, 6);
		assertEquals(22.6, bmi.getBMI(), 0.05);
		assertTrue(bmi.getBMICategory().equalsIgnoreCase("normal"));
	  }

	  @Test
	  public void testGetBMIOverweight()
	  {
		BMI bmi = new BMI(175, 5, 6);
		assertEquals(28.2, bmi.getBMI(), 0.05);
		assertTrue(bmi.getBMICategory().equalsIgnoreCase("overweight"));
	  }

	  @Test
	  public void testGetBMIObese()
	  {
		BMI bmi = new BMI(200, 5, 6);
		assertEquals(32.3, bmi.getBMI(), 0.05);
		assertTrue(bmi.getBMICategory().equalsIgnoreCase("obese"));
	  }
	  
	  @Test
	  public void testInvalidWeight()
	  {
		Set<ConstraintViolation<BMI>> constraintViolations = validator.validateValue( 
			BMI.class, "weight", 20);
		assertEquals(1, constraintViolations.size() );
		assertEquals("Weight must be at least 40lbs", constraintViolations.iterator().next().getMessage() );
	  }
	  @Test
	  public void testInvalidHeightFeet()
	  {
		Set<ConstraintViolation<BMI>> constraintViolations = validator.validateValue( 
			BMI.class, "heightFeet", 2);
		assertEquals(1, constraintViolations.size() );
		assertEquals("HeightFeet must be at least 3 feet", constraintViolations.iterator().next().getMessage() );
	  }
	  @Test
	  public void testInvalidHeightInches()
	  {
		Set<ConstraintViolation<BMI>> constraintViolations = validator.validateValue( 
			BMI.class, "heightInches", 12);
		assertEquals(1, constraintViolations.size() );
		assertEquals("HeightInches must be less than 12 inches", constraintViolations.iterator().next().getMessage() );
	  }

}
